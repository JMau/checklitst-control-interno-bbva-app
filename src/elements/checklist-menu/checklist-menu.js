import { LitElement, html, css } from 'lit-element';  

class ChecklistMenu extends LitElement {

  static get styles() {
    return [
      css`
        :host {
            margin-top: 80px;
        }
      `
    ];
  }

  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }
  
  render() { 
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <div class="d-flex justify-content-between bg-dark">
        <div class="p-2">
          <button type="button" class="btn btn-link" @click="${this.fireEventGoToStart}">Inicio</button>
          <button type="button" class="btn btn-link" @click="${this.fireEventCreateSdaChecklist}">Agregar</button>
        </div>
      </div>
    `;
  } 

  fireEventGoToStart() {
    this.dispatchEvent(new CustomEvent('go-to-start', {}));
  }

  fireEventCreateSdaChecklist() {
    this.dispatchEvent(new CustomEvent('create-sda-checklist', {}));
  }

}  

customElements.define('checklist-menu', ChecklistMenu);