import { LitElement, html, css } from 'lit';
import sandbox from '../../utils/sandbox.js';

export class ChecklistRegister extends LitElement {
  static get properties() {
    return {
      host: { type: String },
      resource: { type: String },
      email: { type: String },
      password: { type: String },
      name: { type: String }
    };
  }

  static get styles() {
    return css`
      :host {
        margin: 0;
        padding: 0;
        background-color: #FFFFFF;
        width: 200vh;
        height: 100vh;
      }
      #login .container #login-row #login-column #login-box {
        margin-top: 120px;
        max-width: 600px;
        height: 400px;
        border: 1px solid #9C9C9C;
        background-color: #EAEAEA;
      }
      #login .container #login-row #login-column #login-box #login-form {
        padding: 20px;
      }
      #login .container #login-row #login-column #login-box #login-form #register-link {
        margin-top: -85px;
      }
    `;
  }

  constructor() {
    super();
    this.email = '';
    this.password = '';
    this.name = '';
    this.host = '';
    this.resource = '/register';
  }

  doRegister(e) {
    e.preventDefault();
    const fullName = this.name.split(' ');
    let initials = '';
    for (let i = 0; i < fullName.length; i++) {
        initials += fullName[i].charAt(0);
    }
    const data = {
      name: this.name,
      initials: initials.toUpperCase(),
      email: this.email,
      password: this.password,
      image: './img/persona.png'
    };
    
    const url = this.host + this.resource;
    const json = JSON.stringify(data);
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.on
    xhr.onload = () => {
      let response = JSON.parse(xhr.responseText);
      if (xhr.readyState === 4 && xhr.status === 201) {
        sandbox.dispatch('login-ok', response, this);
      } else {
        sandbox.dispatch('register-error', response, this);
      }
    }
    xhr.send(json);   
  }

  render() {
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <div id="login">
      <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
          <div id="login-column" class="col-md-6">
            <div id="login-box" class="col-md-12">
              <form id="login-form" class="form">
                <h3 class="text-center text-info">Registrarme</h3>
                <div class="form-group">
                  <label for="name" class="text-info">Nombre:</label><br>
                  <input type="text" name="name" id="name" class="form-control" @input="${this.updateName}" required>
                </div>
                <div class="form-group">
                  <label for="email" class="text-info">Email:</label><br>
                  <input type="text" name="email" id="email" class="form-control" @input="${this.updateEmail}" required>
                </div>
                <div class="form-group">
                  <label for="password" class="text-info">Contrase&ntilde;a:</label><br>
                  <input type="password" name="password" id="password" class="form-control" @input="${this.updatePassword}" required>
                </div>
                <div class="form-group">
                  <br>
                  <a href="javascript:void(0);" class="btn btn-secondary btn-md" @click="${this.goBack}">Regresar</a>
                  <button class="btn btn-info btn-md" @click="${this.doRegister}">Registrar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    `;
  }

  goBack() {
    sandbox.dispatch('hide-register', {}, this);
  }

  updateEmail(e) {
    this.email = e.target.value;
  }

  updateName(e) {
    this.name = e.target.value;
  }

  updatePassword(e) {
    this.password = e.target.value;
  }
}
customElements.define('checklist-register', ChecklistRegister);