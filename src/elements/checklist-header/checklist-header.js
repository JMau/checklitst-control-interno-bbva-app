import { LitElement, html, css } from 'lit-element';  

class ChecklistHeader extends LitElement {

  static get styles() {
    return [
      css``
    ];
  }

  static get properties() {
    return {
      userName: { type: String, attribute: 'user-name' },
      initials: { type: String, attribute: 'initials' },
      title: { type: String, attribute: 'title' },

    };
  }

  constructor() {
    super();
    this.userName = '';
    this.initials = '';
    this.title = 'Control de Checklist Tecnológico';
  }
  
  render() { 
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <div class="d-flex justify-content-between bg-dark text-white">
        <div class="p-2 d-flex">
          <span class="p-2 rounded-circle bg-info">
            <strong>${this.initials}</strong>
        </span>
          <label class="p-2">${this.userName}</label>
        </div>
        <div class="p-2">
          <h3>${this.title}</h3>
        </div>
        <div class="p-2">
          <button type="button" class="btn btn-link" @click="${this.fireEventCloseSession}">Cerrar sesión</button>
        </div>
      </div>
    `;
  } 

  fireEventGoToStart() {
    this.dispatchEvent(new CustomEvent('go-to-start', {}));
  }

}  

customElements.define('checklist-header', ChecklistHeader);