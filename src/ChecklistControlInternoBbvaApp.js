import { LitElement, html, css } from 'lit';
import sandbox from './utils/sandbox.js';

import '../src/elements/checklist-header/checklist-header.js';
import '../src/elements/checklist-menu/checklist-menu.js';
import './elements/checklist-login/checklist-login';
import './elements/checklist-register/checklist-register'

export class ChecklistControlInternoBbvaApp extends LitElement {
  static get properties() {
    return {
      host: { type: String }
    };
  }

  static get styles() {
    return css`
    `;
  }

  constructor() {
    super();
    this.host = "http://localhost:3000";
    this.listenerEvents();
  }

  listenerEvents() {
    sandbox.on('show-register', this.showRegister.bind(this));
    sandbox.on('hide-register', this.hideRegister.bind(this));
    sandbox.on('login-ok', this.loginOk.bind(this));
    sandbox.on('login-error', this.loginError.bind(this));
    sandbox.on('register-error', this.registerError.bind(this));
  }

  hideRegister(e) {
    console.log("Salir de registro");
  }

  showRegister(e) {
    console.log("Mostrar registro");
  }

  render() {
    return html`
    <checklist-login host="${this.host}"></checklist-login>
    <checklist-register host="${this.host}"></checklist-register>
      <checklist-header
        user-name="Mauricio Márquez"
        initials="JM"
        @go-to-start="${() => {}}"></checklist-header>
      <checklist-menu
        @go-to-start="${() => {}}"
        @create-sda-checklist="${() => {}}">
      </checklist-menu>
    `;
  }

  checkLogin() {
    let auth = localStorage.getItem("auth");
    if (auth) {
      this.hasLogin = true;
    }
  }

  loginOk(e) {
    console.log(e);
  }

  loginError(e) {
    alert("Los datos introducidos son incorrectos o el usuario no existe.");
  }

  registerError(e) {
    alert(e + ", please validate.");
  }
}
