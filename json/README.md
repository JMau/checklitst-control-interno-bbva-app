Install json-server and json-server-auth

# NPM
npm install -g json-server json-server-auth
# Yarn
yarn add -D json-server json-server-auth

Run json-server-auth with default port 3000:
json-server-auth db.json -r routes.json

Documentation: https://www.npmjs.com/package/json-server-auth
